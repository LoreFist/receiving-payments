<?php

use yii\log\FileTarget;
use yii\mutex\MysqlMutex;
use yii\queue\db\Queue;
use yii\queue\LogBehavior;
use yii\rbac\DbManager;

$db = require(__DIR__ . DIRECTORY_SEPARATOR . 'db.php');

$params = require(__DIR__ . DIRECTORY_SEPARATOR . 'params.php');

return [
    'name'       => 'receiving-payments',
    'timeZone'   => 'Asia/Novosibirsk',
    'language'   => 'ru-RU',
    'basePath'   => dirname(__DIR__),
    'bootstrap'  => [
        'log',
        'queue',
    ],
    'aliases'    => [
        '@bower'   => '@vendor/bower-asset',
        '@npm'     => '@vendor/npm-asset',
        '@modules' => '@app/modules',
    ],
    'components' => [
        'db'          => $db,
        'log'         => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],

            ],
        ],
        'urlManager'  => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [],
        ],
        'cache'       => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => DbManager::class,
        ],
        'queue'       => [
            'class'     => Queue::class,
            'db'        => 'db',
            'tableName' => '{{%queue}}',
            'channel'   => 'default',
            'mutex'     => MysqlMutex::class,
            'as log'    => LogBehavior::class,
        ],
    ],
    'params'     => $params,
];
