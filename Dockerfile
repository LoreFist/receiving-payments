FROM php:7.4-fpm

RUN docker-php-ext-install pdo pdo_mysql mysqli

WORKDIR /var/www

RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

COPY --chown=www:www . /var/www

USER www

EXPOSE 9000
CMD ["php-fpm"]