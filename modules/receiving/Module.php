<?php

namespace app\modules\receiving;

use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface {

    /**
     * @inheritdoc
     */
    public function bootstrap($app) {

    }
}