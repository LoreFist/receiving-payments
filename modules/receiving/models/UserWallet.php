<?php

namespace app\modules\receiving\models;

/**
 * This is the model class for table "user_wallet".
 *
 * @property int         $id
 * @property int         $user_id    пользователь
 * @property int         $balance    баланс
 * @property string|null $created_at создано
 * @property string|null $updated_at обновлено
 */
class UserWallet extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user_wallet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'balance'], 'required'],
            ['user_id', 'integer'],
            ['balance', 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'         => 'ID',
            'user_id'    => 'User ID',
            'balance'    => 'Balance',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
