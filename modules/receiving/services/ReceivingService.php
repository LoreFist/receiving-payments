<?php

namespace app\modules\receiving\services;

use app\modules\receiving\jobs\ReceivingJob;
use app\modules\receiving\models\UserWallet;
use Yii;
use yii\base\InvalidConfigException;
use yii\queue\Queue;

class ReceivingService {

    /**
     * создает очередь
     *
     * @param Queue $queue
     * @param       $data
     *
     * @return bool
     */
    public static function createJob(Queue $queue, $data) {
        try {
            $job = Yii::createObject([
                'class'      => ReceivingJob::class,
                'userId'     => $data['userId'],
                'sum'        => $data['sum'],
                'commission' => $data['commission'],
            ]);

            return $queue->push($job) ? true : false;
        } catch (InvalidConfigException $e) {
            return false;
        }
    }

    /**
     * генерируем цифровую подпись
     *
     * @return string
     */
    public static function getCrypt() {
        return md5(date('Ymd'));
    }

    /**
     * подсчет баланса кошелька пользователя
     *
     * @param $userId
     * @param $sum
     * @param $commission
     */
    public static function calculationWallet($userId, $sum, $commission) {
        $modelWallet = UserWallet::findOne(['user_id' => $userId]);
        if ( !$modelWallet) {
            $modelWallet          = new UserWallet();
            $modelWallet->user_id = $userId;
        }

        $modelWallet->balance = $modelWallet->balance + ($sum - $sum * ($commission/100));
        $modelWallet->save();
    }
}
