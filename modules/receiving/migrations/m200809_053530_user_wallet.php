<?php

use yii\db\Migration;

/**
 * Class m200809_053530_user_wallet
 */
class m200809_053530_user_wallet extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('user_wallet', [
            'id'         => $this->primaryKey()->unsigned(),
            'user_id'    => $this->integer()->notNull()->comment('пользователь'),
            'balance'    => $this->float()->defaultValue(0)->notNull()->comment('баланс'),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->comment('создано'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('обновлено')
        ], null);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('user_wallet');
    }

}
