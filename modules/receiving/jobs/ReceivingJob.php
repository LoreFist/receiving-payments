<?php

namespace app\modules\receiving\jobs;

use app\modules\receiving\services\ReceivingService;
use yii\base\BaseObject;
use yii\queue\JobInterface;

/**
 * Class SendJob
 *
 * @package app\modules\generate\jobs
 */
class ReceivingJob extends BaseObject implements JobInterface {

    public $userId;
    public $sum;
    public $commission;

    /**
     * @param \yii\queue\Queue $queue
     *
     * @return mixed|void
     */
    public function execute($queue) {
        ReceivingService::calculationWallet($this->userId, $this->sum, $this->commission);
    }
}