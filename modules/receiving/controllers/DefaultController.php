<?php

namespace app\modules\receiving\controllers;

use app\modules\receiving\services\ReceivingService;
use Yii;
use yii\di\Instance;
use yii\queue\Queue;
use yii\web\Controller;

class DefaultController extends Controller {

    /** @var Queue */
    private $queue;

    public function __construct($id, $module, $config = []) {
        $this->queue = Instance::ensure('queue', Queue::class);
        parent::__construct($id, $module, $config);
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        if (
            $data = Yii::$app->request->post() AND
            ReceivingService::getCrypt() == Yii::$app->request->get('crypt')
        ) {
            foreach ($data as $order)
                ReceivingService::createJob($this->queue, $order);

            return true;
        }
    }
}